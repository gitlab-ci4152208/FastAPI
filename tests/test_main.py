from fastapi.testclient import TestClient
from src.main import app

client = TestClient(app)


def test_recipe_id():
    response = client.get("/recipe/0")
    assert response.status_code == 404
    assert response.json() == {"detail": "Recipe with id=0 does not exist."}


def test_recipe():
    response = client.get("/recipe/1")
    assert response.status_code == 200


def test_get_recipes():
    response = client.get("/recipes/")
    print(response.json())
    assert response.status_code == 200
    assert isinstance(response.json(), list)


def test_get_recipes_not_found():
    response = client.get("/recipes/1")
    print(response.json())
    assert response.status_code == 404
    assert response.json() == {"detail": "Not Found"}


def test_post_recipe():
    response = client.post(
        "/recipe/",
        headers={"Content-Type": "application/json", "Accept": "*/*", "Connection": "keep-alive"},
        json={
            "name": "test recipe X",
            "cooking_time": 3.0,
            "ingredients": "some ingredients",
            "description": "descr"
        }
    )
    assert response.status_code == 200
    assert response.json().get("name") == "test recipe X"
