from typing import List
from fastapi import FastAPI, HTTPException
from sqlalchemy.future import select
from contextlib import asynccontextmanager

import models
import schemas
from database import engine, session


@asynccontextmanager
async def lifespan(app: FastAPI):
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)
    yield
    await session.close()
    await engine.dispose()

app = FastAPI(lifespan=lifespan)


@app.post('/recipe/', response_model=schemas.RecipeOut)
async def recipe(recipe: schemas.RecipeIn) -> models.Recipe:
    new_recipe = models.Recipe(**recipe.model_dump())
    async with session.begin():
        session.add(new_recipe)
    return new_recipe


@app.get('/recipe/{id}', response_model=schemas.BaseRecipe)
async def recipe(id: int) -> models.Recipe:
    async with session.begin():
        res = await session.execute(select(models.Recipe).where(models.Recipe.id == id))
        recipe_from_db = res.scalar_one_or_none()

        if not recipe_from_db:
            raise HTTPException(status_code=404, detail=f"Recipe with {id=} does not exist.")

        recipe_from_db.view_plus()
        return recipe_from_db


@app.get('/recipes/', response_model=List[schemas.RecipesForList])
async def recipes():
    async with session.begin():
        res = await session.execute(select(models.Recipe))
    sorted_result = sorted(res.scalars().all(), key=lambda v: (v.views, -v.cooking_time), reverse=True)
    return sorted_result
