from sqlalchemy import Column, String, Integer, Text, Float

from database import Base


class Recipe(Base):
    __tablename__ = 'Recipe'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    cooking_time = Column(Float)
    ingredients = Column(Text)
    description = Column(Text)
    views = Column(Integer, default=0)

    def view_plus(self):
        self.views += 1
